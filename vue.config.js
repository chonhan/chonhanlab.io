module.exports = {
  outputDir: "public",
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'static/index.html',
      filename: 'index.html',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  }
}
